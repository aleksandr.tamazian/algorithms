Сбалансированная команда.

https://codeforces.com/group/pgkaqF4igo/contest/256854/problem/B

Задача на два указаеля.

Time complexity - `O(n * logn)`

Решение со `Scanner` не проходит из-за медленного потокова ввода/ввывода, но принцип алгоритма правильный.
```
void solve() {
    Scanner input = new Scanner(System.in);
    int n = input.nextInt();
    int[] members = new int[n];
    
    prepareData(members, input, n);
    Arrays.sort(members); // O(n  * logn)
    
    int left = 0;
    int result = 0;
    for (int right = 0; right < n; right++) {
            while (members[right] - members[left] > 5) {
                left++;
            }
            result = max(result, right - left + 1);
        }
 
    System.out.print(result + "\n");
}

void prepareData(int[] members, Scanner input, int n) {
        for (int i = 0; i < n; i++) {
            members[i] = input.nextInt();
        }
        Arrays.sort(members);
    }
```