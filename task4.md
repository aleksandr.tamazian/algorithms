Перед экзаменом.

https://codeforces.com/contest/4/problem/B

Сложность - O(n + n) = O(n)

```
static List<Integer> minTime = new ArrayList<>();
    static List<Integer> maxTime = new ArrayList<>();
    static List<Integer> solution = new ArrayList<>();
 
    private static void solve() {
        Scanner input = new Scanner(System.in);
        int d = input.nextInt();
        int sumTime = input.nextInt();
        int min = 0;
        int max = 0;
        
        for (int i = 0; i < d; i++) {
            int minElement = input.nextInt();
            int maxElement = input.nextInt();
 
            minTime.add(minElement);
            maxTime.add(maxElement);
 
            min += minElement;
            max += maxElement;
        }
 
        if (sumTime <= max && sumTime >= min) {
            calculate(d, sumTime - min);
            giveAnswer("YES");
        } else {
            giveAnswer("NO");
        }
    }
 
    private static void calculate(int days, int remain) {
        for (int i = 0; i < days; i++) {
            int difference = maxTime.get(i) - minTime.get(i);
 
            if (remain == 0) {
                solution.add(minTime.get(i));
            } else if (remain > difference) {
                solution.add(maxTime.get(i));
                remain -= difference;
            } else if (remain <= difference) {
                solution.add(remain + minTime.get(i));
                remain = 0;
            }
        }
    }
 
    private static void giveAnswer(String answer) {
        if (answer.equals("YES")) {
            System.out.println("YES");
            solution.forEach(element -> System.out.print(element + " "));
        } else {
            System.out.println("NO");
        }
    }
```
