Массив с нечетной суммой.

https://codeforces.com/contest/1296/problem/A

Решение за `O(n)`:
```
public class Test {

    public static void main(String[] args) {
        solve();
    }

    private static void solve() {
        Scanner input = new Scanner(System.in);
        int testCases = input.nextInt();

        for (int i = 0; i < testCases; i++) {
            if (canRecombineEven(input)) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }

    private static boolean canRecombineEven(Scanner input) {
        int n = input.nextInt();
        int evens = 0;
        int odds = 0;

        for (int i = 0; i < n; i++) {
            if (input.nextInt() % 2 == 0) {
                odds++;
            } else {
                evens++;
            }
        }

        if (evens == n && n % 2 == 0) {
            return false;
        }

        return evens != 0 || odds != n;
    }
}
```
