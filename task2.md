Все делители.

https://codeforces.com/group/pgkaqF4igo/contest/256214/problem/B


Ускоренный поиск всех делителей для числа N.
```
void solve() {
        Scanner input = new Scanner(System.in);
        List<Long> numbers = new LinkedList<>();
        long n = input.nextLong();
        
        for (long i = 1; Math.pow(i, 2) <= n; i++) {
            if (n % i == 0) {
                numbers.add(i);
 
                if (n / i != i) {
                    numbers.add(n / i);
                }
            }
        }
 
        Collections.sort(numbers);
        numbers.forEach(System.out::println);
    }
```
